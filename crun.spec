Name:            crun
Version:         1.8.7
Release:         3
Summary:         A fast and low-memory footprint OCI Container Runtime fully written in C.
URL:             https://github.com/containers/%{name}

Patch1:          0001-tighten-check-in-check_fd_under_path.patch

Source0:         https://github.com/containers/crun/releases/download/%{version}/%{name}-%{version}.tar.xz
License:         GPL-2.0-only
BuildRequires:   autoconf
BuildRequires:   automake
BuildRequires:   gcc
BuildRequires:   git-core
BuildRequires:   gperf
BuildRequires:   libcap-devel
BuildRequires:   systemd-devel
BuildRequires:   yajl-devel
BuildRequires:   libseccomp-devel
BuildRequires:   python3-libmount
BuildRequires:   libtool
BuildRequires:   protobuf-c-devel
%ifnarch riscv64
BuildRequires:   criu-devel
Recommends:      criu
Recommends:      criu-libs
%endif
BuildRequires:   python3
Provides:        oci-runtime

%description
crun is a fast and low-memory footprint OCI Container Runtime fully written in C.

%package help
Summary:         Secondary development document and manual of interface function description.
%description help
Secondary development document and manual of interface function description.

%prep
%autosetup -p1 -n %{name}-%{version}

%build
./autogen.sh
%configure --disable-silent-rules
%make_build

%install
%make_install
rm -rf %{buildroot}%{_prefix}/lib*

%files
%license COPYING
%license COPYING.libcrun
%{_bindir}/%{name}

%files help
%{_mandir}/man1/*

%changelog
* Fri Jul 19 2024 zhangxingrong-<zhangxingrong@uniontech.cn> - 1.8.7-3
- utils: tighten check in check_fd_under_path()

* Sun Apr 28 2024 yinsist <jianhui.oerv@isrc.iscas.ac.cn> - 1.8.7-2
- Disable criu dependency for RISC-V as criu does not currently support RISC-V

* Thu Apr 25 2024 lijian <lijian2@kylinos.cn> - 1.8.7-1
- update to 1.8.7
- crun: new command "crun features".
- linux: support io_priority from the OCI specs.
- cgroup: allow setting swap to 0.
- cgroup, systemd: set the memory limit on the system scope.

* Wed Apr 17 2024 huayumeng <huayumeng@kylinos.cn> - 1.8.1-2
- readonlyPaths should inherit flags from parent mount

* Wed May 10 2023 zmr_2020 <zhang_jian7@hoperun.com> - 1.8.1-1
- update to 1.8.1

* Wed Jul 20 2022 fushanqing <fushanqing@kylinos.cn> - 1.4.5-1
- update to 1.4.5

* Mon May 23 2022 fushanqing <fushanqing@kylinos.cn> - 1.4.3-2
- fix CVE-2022-27650.

* Tue Mar 1 2022 fu-shanqing <fushanqing@kylinos.cn> - 1.4.3-1
- Update to 1.4.3

* Tue Aug 3 2021 fu-shanqing <fushanqing@kylinos.cn> - 0.20.1-1
- Package init
